# Implementation Plan
## 1. Setup Grafana Monitoring
- Download telegraf package and install
```bash
wget https://dl.influxdata.com/telegraf/releases/telegraf_1.20.0-1_amd64.deb

dpkg -i telegraf_1.20.0-1_amd64.deb
```

- Copy and paste below configuration in place
```bash
cat /dev/null > /etc/telegraf/telegraf.conf
nano /etc/telegraf/telegraf.conf
```
```bash
# Telegraf Configuration

[global_tags]
  
[agent]
  interval = "10s"
  round_interval = true
  metric_batch_size = 1000
  metric_buffer_limit = 10000
  collection_jitter = "0s"
  flush_interval = "10s"
  flush_jitter = "0s"
  precision = ""
  hostname = ""
  omit_hostname = false

[[outputs.influxdb]]
  urls = ["http://45.76.153.28:8086"]
  database = "bitkubchain-prod"

[[inputs.cpu]]

  percpu = true
  totalcpu = true
  collect_cpu_time = false
  report_active = false

[[inputs.disk]]
  ignore_fs = ["tmpfs", "devtmpfs", "devfs", "iso9660", "overlay", "aufs", "squashfs"]

[[inputs.diskio]]
  
[[inputs.kernel]]
 
[[inputs.mem]]
 
[[inputs.processes]]

[[inputs.swap]]

[[inputs.system]]

[[inputs.net]]

 interfaces = ["eth0"]

```
- Save and exit
```bash
systemctl start telegraf.conf
systemctl enable telegraf.conf
```
#
## 2. Setup config.toml

```bash
wget https://gitlab.com/ikeowzz/so-implementation/-/raw/main/config.toml
```
```bash
nano config.toml
```
- Please specify `DataDir` at Line 59 and place this file into your directory
```bash
mv config.toml TO_YOUR_SPECIFY_DIRECTORY
```
#
## 3. Setup geth.service
- Stop geth.service
```bash
systemctl stop geth.service
```
- Download new geth.service file
```bash
wget https://gitlab.com/ikeowzz/so-implementation/-/raw/main/geth.service -O /etc/systemd/system/geth.service
```
```bash
chmod +x /etc/systemd/system/geth.service
nano /etc/systemd/system/geth.service
```
#### Please customize below flags and save it
- `--datadir`
- `--unlock`
- `--mine --allow-insecure-unlock --password`
- `--config`
- Save and exit


```bash
systemctl daemon-reload
systemctl disable geth.service
systemctl enable geth.service
systemctl start geth.service
```