# System Requirements

### Linux Instance
- OS - Ubuntu Version 18.04

### Specifications
- 12 vCPUs
- 48 GiB of RAM
- 2 TB of Data Disk (SSD) IOPS: 20,000

### Security Group / Firewall Rules
#### Allow Inbound
- Protocol - TCP & UDP
- Port - 30303
- Source IP - 0.0.0.0/0

### Network Bandwidth
- 50 Mbps/Sec